# timezone

Set timezone

## Requirements

Tested with Ansible 1.8.2

## Role Variables

```
timezone: Europe/Paris
```

Also you can set your timezone like string in *unix directory path format, which be valid from `/usr/share/zoneinfo/`.

## Dependencies

None

## Example Playbook

```
- hosts: all
  gather_facts: False
  pre_tasks:
    - name: pre_tasks | setup python
      raw: command -v yum >/dev/null && yum -y install python python-simplejson libselinux-python redhat-lsb || true ; command -v apt-get >/dev/null && sed -i '/cdrom/d' /etc/apt/sources.list && apt-get update && apt-get install -y python python-simplejson lsb-release aptitude || true
      changed_when: False
    - name: pre_tasks | gets facts
      setup:
      tags:
      - always

  roles:
    - timezone
```

